package com.seibel.lod.core.file.renderfile;

import com.seibel.lod.core.dataObjects.render.ColumnRenderSource;
import com.seibel.lod.core.dataObjects.fullData.sources.ChunkSizedFullDataSource;
import com.seibel.lod.core.pos.DhSectionPos;

import java.io.File;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;

/**
 * This represents LOD data that is stored in long term storage (IE LOD files stored on the hard drive) <br>
 * Example: {@link RenderSourceFileHandler RenderSourceFileHandler} <br><br>
 * 
 * This is used to create {@link ColumnRenderSource}'s 
 */
public interface ILodRenderSourceProvider extends AutoCloseable
{
    CompletableFuture<ColumnRenderSource> read(DhSectionPos pos);
    void addScannedFile(Collection<File> detectedFiles);
    void write(DhSectionPos sectionPos, ChunkSizedFullDataSource chunkData);
    CompletableFuture<Void> flushAndSave();
	
	/** Returns true if the data was refreshed, false otherwise */
    boolean refreshRenderSource(ColumnRenderSource source);
	
	/** Deletes any data stored in the render cache so it can be re-created */
	void deleteRenderCache();
	
}
