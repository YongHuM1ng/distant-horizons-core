package com.seibel.lod.api.enums.worldGeneration;

/**
 * EMPTY, <br>
 * STRUCTURE_START, <br>
 * STRUCTURE_REFERENCE, <br>
 * BIOMES, <br>
 * NOISE, <br>
 * SURFACE, <br>
 * CARVERS, <br>
 * LIQUID_CARVERS, <br>
 * FEATURES, <br>
 * LIGHT, <br>
 *
 *  @author James Seibel
 * @version 2022-7-14
 */
public enum EDhApiWorldGenerationStep
{
	EMPTY,
	STRUCTURE_START,
	STRUCTURE_REFERENCE,
	BIOMES,
	NOISE,
	SURFACE,
	CARVERS,
	LIQUID_CARVERS,
	FEATURES,
	LIGHT,
}
