package com.seibel.lod.api.interfaces.config;

import com.seibel.lod.core.interfaces.dependencyInjection.IBindable;

/**
 * This interface is just used to organize API config groups so
 * they can be more easily handled together.
 * 
 * @author James Seibel
 * @version 9-15-2022
 */
public interface IDhApiConfigGroup extends IBindable
{
	
}
